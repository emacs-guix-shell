;;; emacs-guix-shell - Guix shell integration for Emacs.
;;; Copyright © 2022 David Thompson <davet@gnu.org>
;;;
;;; This file is part of emacs-guix-shell.
;;;
;;; emacs-guix-shell is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU Lesser General Public License
;;; as published by the Free Software Foundation; either version 3 of
;;; the License, or (at your option) any later version.
;;;
;;; emacs-guix-shell is distributed in the hope that it will be
;;; useful, but WITHOUT ANY WARRANTY; without even the implied
;;; warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
;;; See the GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with guile-sdl2.  If not, see
;;; <http://www.gnu.org/licenses/>.

;;; Commentary:
;; To use as the basis for a development environment, run:
;;
;;   guix shell
;;
;; GNU Guix development package.  To build, run:
;;
;;   guix build -f guix.scm
;;
;;; Code:

(use-modules (guix git)
             (guix packages)
             (guix licenses)
             (guix build-system emacs)
             (gnu packages))

(package
  (name "emacs-guix-shell")
  (version "0.1")
  (source (git-checkout (url (dirname (current-filename)))))
  (build-system emacs-build-system)
  (home-page "https://git.dthompson.us/emacs-guix-shell.git")
  (synopsis "Support for 'guix shell'")
  (description
   "This Emacs extension integrates 'guix shell' to set per-buffer
environment variables appropriately.")
  (license gpl3+))
